var url = require('./../../test-url.json');
var fs = require('fs');
var domains = require('./../../domainsToTest.json');
describe('User Centrics app title', function () {

    function openApplicationInBrowser() {
        browser.get(url.e2eUrl);
    }

    function formatDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear(),
            hour = '' + (d.getHours()),
            minutes = '' + d.getMinutes(),
            seconds = d.getSeconds();
        hour = hour < 10 ? '0' + hour : hour;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('') + '_' + [hour, minutes, seconds].join('');
    }

    function todayDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return [year, month, day].join('');
    }

    function takeScreenShot() {
        browser.getCapabilities().then(function (capabilities) {
            var platform = capabilities.get('platform') ? capabilities.get('platform') : '-';
            var browserName = capabilities.get('browserName') ? capabilities.get('browserName') : '-';
            var resolution = capabilities.get('screen_resolution') ? capabilities.get('screen_resolution') : '-';
            var deviceName = capabilities.get('deviceName') ? capabilities.get('deviceName') : '-';
            var version = capabilities.get('version') ? capabilities.get('version') : '-';
            var currenturl = '';
            browser.getCurrentUrl().then(function (value) {
                currenturl = value;
                browser.takeScreenshot().then(function (png) {
                    deviceName = deviceName ? deviceName : platform;
                    var fileDir = url.folderPath + '/' + todayDate();
                    if (!fs.existsSync(fileDir)) {
                        fs.mkdirSync(fileDir);
                    }
                    var passDir = fileDir + '/pass';
                    if (!fs.existsSync(passDir)) {
                        fs.mkdirSync(passDir);
                    }
                    writeScreenShot(png, passDir + '/' + browserName + '_' + version + '_' + resolution + '_' + deviceName + '_'+ 'usercentrics.com' +'_'+ formatDate() + '.png');
                    try {
                        fs.mkdirSync(dir);
                    } catch (e) {
                        if (e.code != 'EEXIST')
                            throw e;
                    }
                    try {
                        // Create your image file if not exists. Image file name is test case name.
                        var stream = fs.createWriteStream(path.join(dir, result.description + '.png'));
                        stream.write(new Buffer(png, 'base64'));
                        stream.end();
                    } catch (e) {
                        if (e.code != 'EEXIST')
                            throw e;
                    }
                });

            });

        }).catch(function (error) {

        });
    }

    function writeScreenShot(data, filename) {
        var stream = fs.createWriteStream(filename);
        stream.write(new Buffer(data, 'base64'));
        stream.end();
    }


    /**
     * Module 6.1 Application should have a title
     */
    // To check the functionality of the Button

    // To check the functionality of Consent Setting Button
    it('Mobilfunkanbieter: günstige Top-Tarife von usercentrics.com', function () {
        browser.get(url.e2eUrl);
        browser.waitForAngularEnabled(false);
        browser.sleep(12000);
        // To click on accept button from the banner
        element(by.css('body > usercentrics-button > div > div:nth-child(1) > app-banner1 > div > div > div.uc-fab-overlay > div > div > div.ic-button.row > div.mobile-button.pt-3.pb-3.col-xl-6.col-lg-6.col-md-6.col-sm-12.col-xs-12 > button')).click();
        // To click on button to open window
        browser.sleep(5000);
        element(by.css('.uc-fab-button.desktop-version')).click();
        takeScreenShot();
        browser.sleep(5000);
        // To Click on Select All check boxes button
        element(by.css('.form-check.cursor-pointer:first-child')).click();
        takeScreenShot();
        browser.sleep(5000);
        // To click on question mark icon to open a Model for details
        element(by.id('optin-help')).click();
        browser.sleep(5000);
        // To click on close button to close the model
        element(by.css('.close-me.cursor-pointer')).click();
        // To click on Update Consent button
        browser.sleep(5000);
        element(by.css('body > usercentrics-button > div > div:nth-child(1) > app-layout1 > div > div > div.card-footer.cursor-pointer.mat-card-footer > div')).click();
        takeScreenShot();
        browser.sleep(5000);
        browser.refresh();
        browser.sleep(5000);
        // To again click on button to check the details
        element(by.css('app-button-svg:first-child')).click();
        browser.sleep(5000);
        takeScreenShot();

    });


});
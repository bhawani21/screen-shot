var cbt = require('cbt_tunnels');
var SpecReporter = require('jasmine-spec-reporter').SpecReporter;
var e2eConfig = require('./test-url.json');
var fs = require('fs');
var path = require('path');
var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
var twilio = require('twilio');
var client = new twilio(e2eConfig.twilio.accountSid, e2eConfig.twilio.authToken);

function todayDate() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('');
}

var fileDir = e2eConfig.folderPath + '/' + todayDate();
if (!fs.existsSync(fileDir)) {
    fs.mkdirSync(fileDir);
}
var failDir = fileDir + '/fail';
if (!fs.existsSync(failDir)) {
    fs.mkdirSync(failDir);
}

var reporter = new HtmlScreenshotReporter({
    dest: failDir,
    captureOnlyFailedSpecs: true
});
exports.config = {

    // change this to your USERNAME and AUTHKEY
    seleniumAddress: 'http://' + e2eConfig.crossBrowserCredentials.username + ':' + e2eConfig.crossBrowserCredentials.password + '@hub.crossbrowsertesting.com:80/wd/hub',

    capabilities: e2eConfig.browsers[0],

    allScriptsTimeout: 30000000,
    maxSessions: 1,
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 250000000,
        isVerbose: true,
        includeStackTrace: true,
        print: function () {
        }
    },

    //
    specs: ['spec/e2e/*.js'],
    beforeLaunch: function () {
        console.log("Connecting local");
        return new Promise(function (resolve, reject) {
            cbt.start({
                "username": e2eConfig.crossBrowserCredentials.username,
                "authkey": e2eConfig.crossBrowserCredentials.password
            }, function (err) {
                if (!err) {
                    console.log('Connected. Now testing...');
                    resolve();
                }
                else {
                     return reject(err);
                }
            });
        });
    },
    onPrepare: function () {
        console.log('onPrepare method called');
        // jasmine.getEnv().addReporter(new SpecReporter({
        //     specDone: function (result) {
        //         console.log('------ 44 -----');
        //         client.messages.create({
        //             from: "+4915735981321",
        //             to: "+919351297565",
        //             body: "You just sent an SMS from Node.js using Twilio!"
        //         }).then(function (messsage) {
        //             console.log('------+ 222++++++++++++++++++-----');
        //             console.log(messsage);
        //         }).catch(function(err){
        //             console.log('err--- 11 ------->', err);
        //         });
        //         if (result.failedExpectations.length > 0) {
        //
        //             browser.getProcessedConfig().then(function (config) {
        //                 browser.takeScreenshot().then(function (png) {
        //                     var dirPath = './reports/screenshots/';
        //                     if (!fs.existsSync('./reports')) {
        //                         fs.mkdirSync('./reports');
        //                         if (!fs.existsSync(dirPath))
        //                             fs.mkdirSync(dirPath);
        //                     }
        //                     var fileName = (config.capabilities.browserName + '-' + result.fullName).replace(/[\/\\]/g, ' ').substring(0, 230);
        //                     var stream = fs.createWriteStream(dirPath + fileName + '.png');
        //                     stream.write(new Buffer(png, 'base64'));
        //                     stream.end();
        //                 }, function (error) {
        //                     console.log("failed to take screenshot");
        //                 })
        //             })
        //         }
        //     },
        //     spec: {
        //         displayStacktrace: false
        //     }
        // }));
        jasmine.getEnv().addReporter(new function() {
            console.log('-------test started11');
            this.specDone = function(result) {
                console.log('-------test started22');
                if (result.failedExpectations.length > 0) {
                    console.log('-------test failed');
                    browser.getProcessedConfig().then(function (config) {
                        browser.takeScreenshot().then(function (png) {
                            var dirPath = './reports/screenshots/';
                            if (!fs.existsSync('./reports')) {
                                fs.mkdirSync('./reports');
                                if (!fs.existsSync(dirPath))
                                    fs.mkdirSync(dirPath);
                            }
                            var fileName = (config.capabilities.browserName + '-' + result.fullName).replace(/[\/\\]/g, '_').substring(0, 230);
                            var stream = fs.createWriteStream(dirPath + fileName + '.png');
                            stream.write(new Buffer(png, 'base64'));
                            stream.end();
                        }, function (error) {
                            console.log("failed to take screenshot");
                        })
                    });
                            client.messages.create({
                                body: "You just sent an SMS from Node.js using Twilio!",
                                from: "+4915735981321",
                                to: "+919351297565"
                            }).then(function (message) {
                                console.log('------+ 222++++++++++++++++++-----');
                                console.log(message);
                            }).catch(function(err){
                                console.log('err--- 11 ------->', err);
                            });
                }
            };
        });
        jasmine.getEnv().addReporter(reporter);
    }
};